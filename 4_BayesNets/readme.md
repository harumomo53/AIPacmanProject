Bayes' Nets
===
I implemented inference algorithms for Bayes Nets, specifically **variable elimination** and **likelihood weighting sampling**.

## Superpowered Pacman
Pacman will have to choose its own set of superpowers without knowing the superpowers the ghosts have chosen. However, Pacman will get to see some clues about the ghosts that can help him figure out what superpowers they have using probabilistic inference.

- Both Pacman and the ghosts can either shoot lasers('n'), trigger ultrasonic blasts('b'), or become really fast.
- ```python pacman.py -w blast=2,laser=1 -s "{blast=1,laser=1},{speed=2}"```: 2 means powerful and 1 means weak. So Pacman has a powerful blast and a weak laser. The first ghost gets a weak blast and a weak laser and the second ghost gets a powerful speed.

## Q1. Join Factors
- Implemented ```joinFactors()``` in ```factorOperations.py```. It takes in a list of ```Factors``` and returns a new ```Factor``` whose variables(probability entries) are the product of the corresponding rows of the input ```Factors```.

### How to run
- ```python autograder.py -q q1```
- ```python autograder.py -t test_cases/q1/1-product-rule```



## Q2. Eliminate
- Implemented ```eliminate()``` function in ```factorOperations.py```. It takes a Factor and a variable to eliminate and returns a new Factor that doesn't contain that variable. 
- This is **summing all of the entries in the Factor which only differ in the value of the variable being eliminated**.

### How to run
- ```python autograder.py -q q2```
- ```python autograder.py -t test_cases/q2/1-simple-eliminate```



## Q3. Normalize
- Implemented ```normalize``` function in ```factorOperations.py```. It takes a Factor as input and normalizes it (= it **scales all of the entries in the Factor such that the sum of the entries in the Factor is 1**.)

### How to run
- ```python autograder.py -q q3```



## Q4. Variable Elimination
- This is more efficient way of solving inference by enumeration.
- Implemented the ```inferenceByVariableElimination()``` function in ```inference.py```. It answers a probabilistic query, which is represented using a ```BayesNet```, a list of query variables, and the evidence.
- Algorithm iterates over **hidden variabless** in elimination order, performing joining and eliminating that variable, until the query and evidence variables are the only variables left.

### How to run
- ```python autograder.py -q q4```


## Q5. Speed up Inference
If one of two conditions hold, a node *X* in a BN can be ignored.

1. If the query variables are conditionally independent of *X* given the evidence variable, *X* can be ignored.
2. If *X* and all of its descendents are unconditioned and non-query variables, *X* and its descendants will be eliminated and will sum to 1, and thus will not affect the query.

### Q1
![Q1](https://github.com/eunjincho503/AIPacmanProject/blob/master/4_BayesNets/img/q1.png)

- In this question, query **T** is independent of **W** given **D** according to d-separation.
- Also query **G** is independent of **E** and **T** given **F** according to d-separation rule. Therefore both **W** and **G** can be ignored for a query **P(T|d,f)**

### Q2
![Q2](https://github.com/eunjincho503/AIPacmanProject/tree/master/4_BayesNets/img/q2.png)
- **H** and **I**: independent of **C** given **E** according to same d-separation rule for the Q1.
- **D**: dependent of **C**
- **A**: independent of **G** given **F**

### Q3
![Q3](https://github.com/eunjincho503/AIPacmanProject/tree/master/4_BayesNets/img/q3.png)
- **Y7**, **Y4**, **Y3**, **X6**, **X5**, **X4**, **X2** : independent of **Y9** given **Z**
- **Y13**, **Y12** : independent of **Y9** given **Y10**


## Q6. Power Prediction
This is to use inference to help Pacman defeat the superpowered ghosts. Packman knows that the ghosts choose their powers probabilistically according to the following BN.

![pacman](https://github.com/eunjincho503/AIPacmanProject/tree/master/4_BayesNets/img/pacman.png)

- A ```ReflexPowerChoosingAgent``` will observe **time, temperature, size of ghosts, and whether or not the ghosts have belts** = evidence variables
- ```getPowerChoice()```: choose a power for Pacman that will counter the ghosts' powers. 


## Q7. Likelihood Weighting Sampling
Implemented ```inferenceByLikelihoodWeightingSampling()``` function in ```inference.py```.








