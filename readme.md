Pacman Project
===

In this Pacman porject, we learned foundation AI concepts, such as informed state-space search, probabilistic inference, and reinforcement learning. 

It's written in pure Python 2.7. 

## Project Overview
### Search 
Implemented **depth-first**, **breadth-first**, **uniform cost**, and **A-star search algorithms**. These algorithms are used to solve navigation and travelling salesman problems in the Pacman world.

### Multi-Agent search
Pacman is modeled as an adversarial and stochastic search problem. I implemented multiagent **minimax** and **expectimax algorithms**, as well as designing **evaluation functions**.

### Reinforcement learning
Implemented **model-based** and **model-free** reinforcement learning algorithms.

### Ghostbusters
**Probabilistic inference** in a **hidden Markov model** tracks the movement of hidden ghosts in the Pacman world. We implemented **exact inference** using the **forward algorithm** and **approximate inference** using **particle filters**.  

### Classification
Implemented standard machine learning classification algorithms using **Naive Bayes**, **Perceptron**, and **MIRA** modles to classify digits. Also extends this by implementing a **behavioral cloning** Pacman agent.
