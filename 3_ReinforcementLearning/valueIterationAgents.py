# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        # util.Counter().argMax() -> returns the key with max value
        values_copy = util.Counter() # off-line learning

        # No "return", Updates the self.values for each state in the grid under given iterations
        for i in range (0, iterations):
            # Given vector of V_k(s) values
            states = mdp.getStates()
            # Do one ply of expectimax from each state
            for state in states:
                legal_actions = mdp.getPossibleActions(state)
                q_values = util.Counter()
                # for action in legal_actions
                for action in legal_actions:
                    # compute T(s,a,s')[R(s,a,s')+GVk(s')] and add it to total_sum
                    q_values[action] = self.computeQValueFromValues(state, action)
                # q_values = {action: value}. Now we want the max action using argMax()
                max_action = q_values.argMax()
                values_copy[state] = q_values[max_action] # values_copy = {state: value}
            self.values = values_copy.copy()

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.

          Q-VALUE
        """
        "*** YOUR CODE HERE ***"
        # Compute Q*(s,a) = sum_{s'}T(s,a,s')[R(s,a,s') + gamma V*(s')]
        val_toRtn = 0.0
        new_states = self.mdp.getTransitionStatesAndProbs(state, action) # (next_state, prob) list
        for next_state, prob in new_states:
            val_toRtn += prob * (self.mdp.getReward(state, action, next_state) + (self.discount * self.values[next_state]))
        return val_toRtn

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.

          ACTION
        """
        "*** YOUR CODE HERE ***"
        # If no legal actions, return None
        actions = self.mdp.getPossibleActions(state)
        if len(actions) == 0:
            return None
        # Else, compute best action in the given state
        action_dict = util.Counter()
        for action in actions:
            val_toRtn = 0.0
            new_states = self.mdp.getTransitionStatesAndProbs(state, action)
            for next_state, prob in new_states:
                val_toRtn += prob * (self.mdp.getReward(state, action, next_state) + (self.discount * self.values[next_state]))
            action_dict[action] = val_toRtn
        return action_dict.argMax()

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
