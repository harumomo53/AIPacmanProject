Reinforcement Learning
===

## Q1. Value Iteration
- ```ValueIterationAgent``` takes an MDP on construction and runs value iteration for the specified number of iterations before the constructor returns.

- ```init()```: 
    - For every state, we need to update. And for each state in the grid, there are 4 legal actions. 
    - So we calculate the q-value of this **state** and **action**. 
    - Using custom dictionary, we collect all 4 different q-values under 4 different action and decide which is the greatest. Find the value according to that maximizing action.
    - Set self.values = {state: value} 

- For the 3rd step in ```self.init()```, we need ```self.computeQValueFromValues(state, action)``` method. It's more like a helper method, which calculates the sum of all values given state and action.

- ```self.computeActionFromValues(state)``` method is trying to get the **action** instead of **value**. So while  ```self.computeQValueFromValues(state, action)``` is computing **value** according to the state and action, ```self.computeActionFromValues(state)``` is trying to get the maximizing **action** by looping over the action of that state and return the action.

### How to run
- ```python autograder.py -q q1```


## Q2. Bridge Crossing Analysis
In order to make agent to cross the bridge, I needed to eliminate the noise. Because of the noise, the agents sees that it might fall into the chasm even if it tried to go forward. For this risk, it's acting more risk-aversely, not trying to cross the bridge. This is why I reduced the noise to 0.

## Q3. Policies
a. Prefer the close exit(+1), risking the cliff(-10)
    - Low living reward (Prefer the close exit(+1)때문) = 0.0
    - Low noise (risking the cliff(-10)때문) = 0.0
    - Low discount (Prefer the close exit(+1)때문) = 0.05
b. Prefer the close exit (+1), but avoiding the cliff (-10)
    - Low living reward (Prefer the close exit(+1)때문) = 0.0
    - High noise (avoiding the cliff (-10)때문) = 0.1
    - Low discount (Prefer the close exit(+1)때문) = 0.1
c. Prefer the distant exit (+10), risking the cliff (-10) 
    - High living reward (Prefer the distant exit(+1)때문) = 0.3
    - Low noise (risking the cliff (-10)때문) = 0.0
    - High discount (Prefer the distant exit (+10)때문) = 0.7
d. Prefer the distant exit (+10), avoiding the cliff (-10)
    - High living reward (Prefer the distant exit (+10)때문) = 0.7
    - High noise (avoiding the cliff (-10)때문) = 0.3
    - High discount (Prefer the distant exit (+10)때문) = 0.3
e. Avoid both exits and the cliff (so an episode should never terminate) 
    - Very High living reward = 1.0
    - Very High noise = 1.0
    - Very High discount = 1.0

## Q4. Q-Learning
- In real world, we don't know the MDP model with Transition function and Reward function. We need to actually **learn from experience**. 
- Q-learning agent learns by **trial and error** from interactions with the environemnt through its ```update(state, action, nextState, reward)``` method. 

### Methods
- ```update(state, action, nextState, reward)``` : update Q-value
- ```computeValueFromQValues(state)``` : find max value over legal actions 
- ```getQValue(state, action)``` : find q-value in the dictionary
- ```getAction(state)``` : Depending on the probability ```self.epsilon```, decide what action to take. Optimal action selection takes place in ```computeActionFromQValues(state)```.
- ```computeActionFromQValues(state)``` : compute the best action to take in a state
![Q-learning]()

### How to run
```python autograder.py -q q4```


## Q5. Epsilon Greedy
- Implement epsilon-greedy action selection in ```getAction```. It chooses random actions an epsilon fraction of he time, and follows its current best Q-values otherwise. 


## Q6. Bridge Crossing 2
- It was **not possible** to successfully find the optimal path with certain epsilon(exploration rate) and alpha(learning rate) under 50 iterations.

## Q8. Approximate Q-Learning
- Instead of epsilon greedy, **exploration function** works better when deciding what to experience + It's more efficient because of the feature function.
- An approximate Q-learning agent learns **weights for features of states**. 
- Approximate Q-Learning assumes the existence of **feature function f(s,a)**. It is ```util.Counter``` objects containing non-zero pairs of features and values.
- weight vector: {features: weight}













