1. Search
===

My Pacman agent will find paths through his maze world to reach a particular location and to collect food efficiently by using general search aglorithms.

## Depth First Search
### How to run
A good DFS implementation will quickly find a solution for:

- ```python pacman.py -l tinyMaze -p SearchAgent -a fn=tinyMazeSearch``` : tells the *SearchAgent* to use *tinyMazeSearch* as its search algorithm, which is implemented in ```serach.py```.
- ```python pacman.py -l tinyMaze -p SearchAgent```
- ```python pacman.py -l mediumMaze -p SearchAgent```
- ```python pacman.py -l bigMaze -z .5 -p SearchAgent```

### Important Notes
- Search funtions should return a list of *actions*(legal moves) that will lead the agent from the start to the goal.
- Used a ```Stack```, ```Queue``` and ```PriorityQueue``` from ```util.py```
- It's a graph search algorithm that avoids expanding any already visited states

### Pseudocode
(https://en.wikipedia.org/wiki/Depth-first_search)
```
procedure DFS-iterative(G, v):
    let S be a stack
    S.push(v)
    while S is not empty
        v = S.pop()
        if v is not labeled as discovered:
            label v as discovered
            for all edges from v to w in G.adjacentEdges(v) do
                if w is goal
                    return path
                else 
                    S.push(w)
```


## Breadth First Search
### How to run
A good BFS implementation will quickly find a solution for:

- ```python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs```
- ```python pacman.py -l bigMaze -p SearchAgent -a fn=bfs -z .5```
- Eight-puzzle search problem : ```python eightpuzzle.py```


## Uniform-cost Graph Search Algorithm
By changing the cost function, we can encourage Pacman to find different paths. For example, we can charge more for dangerous steps in ghost-ridden areas or less for steps in food-rich areas.

### How to run
Below are all UCS agents that differ only in the cost function they use

- ```python pacman.py -l mediumMaze -p SearchAgent -a fn=ucs```
- ```python pacman.py -l mediumDottedMaze -p StayEastSearchAgent```
- ```python pacman.py -l mediumScaryMaze -p StayWestSearchAgent```

## A-star Search
A-star takes a heuristic function as an argument. A-star serach finds the optimal solution slightly faster than uniform cost search.

### How to run
- Manhattan distance heuristic: ```python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic```


## Corners Problem (BFS)
In *corner mazes*, there are 4 dots, one in each corner. Our new search problem is to **find the shortest path** through the maze that touches all 4 corners. 

- The only parts of the game state we need to reference = **starting Pacman position** and the **location of 4 corners**.

### How to run
- ```python pacman.py -l tinyCorners -p SearchAgent -a fn=bfs,prob=CornersProblem```
- ```python pacman.py -l mediumCorners -p SearchAgent -a fn=bfs,prob=CornersProblem```


## Corners Problem (A-star)
Implemented a non-trivial, consistent heuristic for the ```CornersProblem``` in ```cornerHeuristic```. 

### How to run
- ```python pacman.py -l mediumCorners -p AStarCornersAgent -z 0.5```

### Admissibility, Consistency and Non-Triviality
- Heuristics are functions that take **search states** and return **numbers that estimate the cost to a nearest goal**. Effective heuristics will return values **closer to the actual goal costs**. 

1. **Admissible**
- The heuristic values must be **lower bounds** on the actual shortest path cost to the nearest goal and non-negative.

2. **Consistent**
- H(from) - H(to) <= C(from, to)

3. **Non-Trivial**
- We want heuristic that reduces total compute time.

- We need both admissibility and consistency satisfied for a good heuristic. Usually, admissible heuristics are consistent. But we can check if it's inconsistent by exploring each node we expand. Also, if UCS and A-star return paths of different lenghs, our heuristic is inconsistent.


## Eating All dots (A-star)
- We want our Pacman to eat all the food in as few steps as possible. For this, we need a new search problem definition which formalizes the food-clearing problem (```FoodSearchProblem``` in ```searchAgents.py```)
- For the present project, solutions do not take into account any ghosts or power pellets; solutions only depend on **the placement of walls, regular food and Pacman**.
- Filled in ```foodHeuristic``` in ```searchAgents.py``` with a consistent heuristic for the ```FoodSearchProblem```.

### How to run
- ```python pacman.py -l trickySearch -p AStarFoodSearchAgent```
- ```python pacman.py -l testSearch -p AStarFoodSearchAgent```


## Suboptimal Search
To find a reasonably good path quickly, I write an agent that always greedily eats the closest dot. ```ClosestDotSearchAgent``` is implemented in ```searchAgents.py``` with a key function that finds a path to the closest dot.

### How to run
- ```python pacman.py -l bigSearch -p ClosestDotSearchAgent -z .5```

















