Multi-Agent Pacman
===
Design of the classic version of Pacman, including ghosts. This implementation includes both minimax and expectimax search and try my hand at evaluation function design.

## Q1. Reflex Agent
Improved the ReflexAgent in ```multiAgents.py```. A capable reflex agent will have to consider both **food locations** and **ghost locations** to perform well. 

### Important Notes
- Features use the **reciprocal** of important values such as distance to food rather than the values themselves.
- The evaluation function I'm writing is evaluating **state-action pairs**
- **I needed the different weights of features depending on feature's importance in the game**. In this game, eating the dots were a lot more integral that avoiding the ghost.

### How to run
- ```python pacman.py -p ReflexAgent -l testClassic```
- ```python pacman.py --frameTime 0 -p ReflexAgent -k 1```
- ```python pacman.py --frameTime 0 -p ReflexAgent -k 2```
- ```python autograder.py -q q1```



## Q2. Minimax
- Implements ```MinimaxAgent``` in ```multiAgents.py```. This minimax agent works with any number of ghosts. In particular, my minimax tree will have **multiple min layers** (one for each ghost) for every max layer.
- My code expands the game tree to an arbitrary depth. The leaves of my minimax tree will be scored with the supplied ```self.evaluationFunction```, which defaults to ```scoreEvaluationFunction```. 
- ```MinimaxAgent``` is a subclass of ```MultiAgentSearchAgent```, which gives access to ```self.depth``` and ```self.evaluationFunction```.
- A single search = 1 Pacman move + all the ghosts' responses. e.g. depth 2 search = 2(Pacman + each ghost moving).

### Important Notes
- The correct implementation of minimax sometimes lead to Pacman losing the game.
    - ```python pacman.py -p MinimaxAgent -l trappedClassic -a depth=3```
    - This is wrong thing to do with random ghosts, but minimax agent always assume the worst. Make sure why.
- In ```self.evaluationFunction```, we're evaluation **states** rather than **actions** or **state-action pairs**, as we were for the reflex agent. 
- All states in minimax should be ```GameStates```, either passed in to ```getAction``` or generated via ```GameState.generateSuccessor```.
- I was a bit foolish because I was trying to save all the list of actions. It turned out to be unnecessary. Just find the value which is the best possible value and save it as a dictionary {val : one action}

### How to run
- ```python autograder.py -q q2```



## Q3. Alpha-Beta Pruning
- This is to more efficiently explore the minimax tree, in ```AlphaBetaAgent```. 
- Should extend to more general alpha-beta pruning logic that is more appropriate to multiple minimizer agents
- Ideally, depth 3 on ```smallClassic``` should run in just a few seconds per move or faster
- succssor states should be processed in the order returned by ```GameState.getLegalActions```
- Did not prune on equality 

### How to run
- ```python pacman.py -p AlphaBetaAgent -a depth=3 -l smallClassic```
- ```python autograder.py -q q3```



## Q4. Expectimax
- Both minimax and alpha-beta assume that we are playing against an adversary who makes **optimal decisions**. In reality, however, this is not always the case. So I implemented the ```ExpectimaxAgent```, which is useful for **modeling probabilistic behavior of agents who may make suboptimal choices**.
- Expectimax no longer takes the min over all ghost actions, but the expectation according to your agent's model of how the ghosts act. (Assume I will only be running against an adversary which chooses amongst their ```getLegalActions``` uniformly at random)

### How to run
- In order to test the general applicability of expectimax, these test cases are based on generic trees
    - ```python autograder.py -q q4```
- ```python pacman.py -p ExpectimaxAgent -l minimaxClassic -a depth=3```
- Here we can see ghosts' more cavalier approach. So if Pacman perceives that he could be trapped but might escape to grab a few more pieces of food, he'll at least try. Compare the below:
    - ```python pacman.py -p AlphaBetaAgent -l trappedClassic -a depth=3 -q -n 10```
    - ```python pacman.py -p ExpectimaxAgent -l trappedClassic -a depth=3 -q -n 10```



## Q5. Evaluation Function
- I wrote a better evaluation function for pacman in function ```betterEvaluationFunction```.
- The evaluation function should evaluate states, rather than actions like your reflex agent evaluation function did.

### How to run
- ```python autograder.py -q q5```




