# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        - remaining food (newFood)
        - Pacman position after moving (newPos).
        - newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"

        # Number of food (Smaller, better)
        food_left = successorGameState.getNumFood()

        # Find the distance between closest food and agent (Shorter, better)
        foodGrid = newFood.asList()
        dist_to_closest_food = float('inf')
        for food in foodGrid:
            dist = util.manhattanDistance(newPos, food)
            if dist < dist_to_closest_food:
                dist_to_closest_food = dist
        if dist_to_closest_food == float('inf'):
            dist_to_closest_food = 0

        # Find the distance between closest ghost and agent (Larger, better)
        dist_to_ghost = util.manhattanDistance(newPos, newGhostStates[0].getPosition())

        finalScore = 0
        # if the ghost is too close, freeze negative negative score. don't move to there
        if dist_to_ghost < 2:
            finalScore = -float('inf')
            return finalScore
        else:
            # Important thing here was that you have to have different "weight" depending on the importance
            # Like advised, I used the reciprocals of important information
            finalScore = (1.0/(dist_to_closest_food+1))*500 + (1.0/(food_left+1))*5000000 + dist_to_ghost
            return finalScore


def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)
        self.totalNumAgents = 0

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
      it's a subclass of MultiAgentSearchAgent
    """

    # I should keep these in mind:
    #   - whether current agent is min agent/ max agent (by incrementing agentNum)
    #   - the depth (to check if this is the terminal node)

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        # set self.totalNumAgents
        self.totalNumAgents = gameState.getNumAgents()
        actionDict = dict() # key = action, value = minimax value

        for action in gameState.getLegalActions(self.index):
            curDepth = 0
            successorState = gameState.generateSuccessor(self.index, action)
            print "ACTION: " , action
            action_val = self.value(successorState, self.index+1, curDepth+1)
            actionDict[action] = action_val

        print actionDict
        # find the max among actionDict and return the key (action)
        return max(actionDict, key=actionDict.get)


    def value(self, curState, curAgentNum, curDepth):

        if (curAgentNum == self.totalNumAgents):
            curAgentNum = 0

        '''
        - Basically, self.depth may not be the real depth of the whole tree. That's why we're using self.evaluationFunction()
        1. curState.isWin() or curState.isLose() -> where all the utils lie at the leaf nodes
        e.g. curDepth:  3  self.totalNumAgents:  2  self.depth:  3  curState.iswin():  True  curState.isLose():  False
                        /-----a------\
                       /              \
                      /                \
                    b1                  b2
                     |                /    \
                    cx             c3        c4
                     |            /   \     /   \
                    dx           d5   d6   d7   d8
                   4.01          4    -7   0    5

        2. curDepth == self.depth * self.totalNumAgents -> stop cuz we don't want to go deeper
        - 1 Depth = one Pacman move + all ghosts' responses
          (so depth2 actually means 2 * all number of agents in a tree)
        e.g. curDepth:  2  curAgent:  0  self.totalNumAgents:  2  self.depth:  1  curState.isWin():  False
                            max
                           /   \
                        min1    min2
                         |      /  \
                         A      B   C
                        10     10   0
        '''
        print "curDepth: ", curDepth , " curAgent: ", curAgentNum, " self.totalNumAgents: " , self.totalNumAgents , " self.depth: ", self.depth
        # if the state is a terminal state, return state's utility
        if (curDepth == self.depth*self.totalNumAgents) or curState.isWin() or curState.isLose():
            print "terminal: " ,self.evaluationFunction(curState) , "isWin: ", curState.isWin()
            return self.evaluationFunction(curState)

        # if the agent is MAX, return max_value()
        elif (curAgentNum == 0):
            print "MAX agent: "
            return self.max_value(curState, curAgentNum, curDepth)

        # if the agent is MIN, return min_value()
        elif (curAgentNum >= 1):
            print "MIN agent: "
            return self.min_value(curState, curAgentNum, curDepth)


    def max_value(self, curState, curAgentNum, curDepth):
        v = -float('inf')
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            successorState = curState.generateSuccessor(curAgentNum, action)
            v = max(v, self.value(successorState, curAgentNum+1, curDepth+1))
        return v


    def min_value(self, curState, curAgentNum, curDepth):
        v = float('inf')
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            successorState = curState.generateSuccessor(curAgentNum, action)
            v = min(v, self.value(successorState, curAgentNum + 1, curDepth + 1))
        return v


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        # set self.totalNumAgents
        self.totalNumAgents = gameState.getNumAgents()
        actionDict = dict()  # key = action, value = minimax value

        alpha = -float('inf')
        beta = float('inf')
        for action in gameState.getLegalActions(self.index):
            curDepth = 0
            successorState = gameState.generateSuccessor(self.index, action)
            action_val = self.value(successorState, self.index + 1, curDepth + 1, alpha, beta)
            alpha = max(alpha, action_val)
            actionDict[action] = action_val

        # find the max among actionDict and return the key (action)
        return max(actionDict, key=actionDict.get)

    def value(self, curState, curAgentNum, curDepth, alpha, beta):

        if (curAgentNum == self.totalNumAgents):
            curAgentNum = 0

        # if the state is a terminal state, return state's utility
        if (curDepth == self.depth * self.totalNumAgents) or curState.isWin() or curState.isLose():
            return self.evaluationFunction(curState)

        # if the agent is MAX, return max_value()
        elif (curAgentNum == 0):
            return self.max_value(curState, curAgentNum, curDepth, alpha, beta)

        # if the agent is MIN, return min_value()
        elif (curAgentNum >= 1):
            return self.min_value(curState, curAgentNum, curDepth, alpha, beta)

    def max_value(self, curState, curAgentNum, curDepth, alpha, beta):
        v = -float('inf')
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            successorState = curState.generateSuccessor(curAgentNum, action)
            v = max(v, self.value(successorState, curAgentNum + 1, curDepth + 1, alpha, beta))
            # if v > beta, then min will not choose it nmw
            if v > beta:
                return v
            alpha = max(alpha, v)
        return v

    def min_value(self, curState, curAgentNum, curDepth, alpha, beta):
        v = float('inf')
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            successorState = curState.generateSuccessor(curAgentNum, action)
            v = min(v, self.value(successorState, curAgentNum + 1, curDepth + 1, alpha, beta))
            # if v < alpha, then max will not choose it nmw
            if v < alpha:
                return v
            beta = min(beta, v)
        return v



class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        self.totalNumAgents = gameState.getNumAgents()
        actionDict = dict()  # key = action, value = minimax value

        for action in gameState.getLegalActions(self.index):
            curDepth = 0
            successorState = gameState.generateSuccessor(self.index, action)
            action_val = self.value(successorState, self.index + 1, curDepth + 1)
            actionDict[action] = action_val

        # find the max among actionDict and return the key (action)
        return max(actionDict, key=actionDict.get)

    def value(self, curState, curAgentNum, curDepth):

        if (curAgentNum == self.totalNumAgents):
            curAgentNum = 0

        # if the state is a terminal state, return state's utility
        if (curDepth == self.depth * self.totalNumAgents) or curState.isWin() or curState.isLose():
            return self.evaluationFunction(curState)

        # if the agent is MAX, return max_value()
        elif (curAgentNum == 0):
            return self.max_value(curState, curAgentNum, curDepth)

        # if the agent is MIN, return min_value()
        elif (curAgentNum >= 1):
            return self.exp_value(curState, curAgentNum, curDepth)

    def max_value(self, curState, curAgentNum, curDepth):
        v = -float('inf')
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            successorState = curState.generateSuccessor(curAgentNum, action)
            v = max(v, self.value(successorState, curAgentNum + 1, curDepth + 1))
        return v

    def exp_value(self, curState, curAgentNum, curDepth):
        v = 0
        # for each successor, get max
        for action in curState.getLegalActions(curAgentNum):
            # p = probability(successor)
            p = 1.0/float(len(curState.getLegalActions(curAgentNum)))
            successorState = curState.generateSuccessor(curAgentNum, action)
            v += p * self.value(successorState, curAgentNum + 1, curDepth + 1)
        return v

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
      1. Number of food -> smaller, better
      2. Distance to closest food -> shorter, better
      3. distance to closes ghost -> further, better
        - if the ghost is too short, AVOID!
      4. Number of capsules left -> smaller, better
    """

    # Useful information you can extract from a GameState (pacman.py)
    newPos = currentGameState.getPacmanPosition()
    newFood = currentGameState.getFood()
    newGhostStates = currentGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    "*** YOUR CODE HERE ***"

    # Number of food (Smaller, better)
    food_left = currentGameState.getNumFood()
    capsules_left = len(currentGameState.getCapsules())

    # Find the distance between closest food and agent (Shorter, better)
    foodGrid = newFood.asList()
    dist_to_closest_food = float('inf')
    for food in foodGrid:
        dist = util.manhattanDistance(newPos, food)
        if dist < dist_to_closest_food:
            dist_to_closest_food = dist
    if dist_to_closest_food == float('inf'):
        dist_to_closest_food = 0

    # Find the distance between closest ghost and agent (Larger, better)
    dist_to_ghost = util.manhattanDistance(newPos, newGhostStates[0].getPosition())

    finalScore = 0
    # if the ghost is too close, freeze negative negative score. don't move to there
    if dist_to_ghost < 1.5:
        finalScore = -float('inf')
        return finalScore
    else:
        # Important thing here was that you have to have different "weight" depending on the importance
        # Like advised, I used the reciprocals of important information
        finalScore = (1.0 / float(dist_to_closest_food + 1)) * 500 + (1.0 / float(food_left + 1)) * 5000000 + dist_to_ghost + (1.0/float(capsules_left+1)) * 1000
        return finalScore

# Abbreviation
better = betterEvaluationFunction

