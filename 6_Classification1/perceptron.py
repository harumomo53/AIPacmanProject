# Perceptron implementation
import util

class PerceptronClassifier:
    """
    Perceptron classifier.

    Note that the variable 'datum' in this code refers to a counter of features
    (not to a raw samples.Datum).
    """
    def __init__( self, legalLabels, max_iterations):
        self.legalLabels = legalLabels
        self.type = "perceptron"
        self.max_iterations = max_iterations
        self.automaticTuning = False # Flat for automatic tuning of the parameters
        self.weights = {}  # {(label) : {feature: value}}
        for label in legalLabels:
            self.weights[label] = util.Counter() # this is the data-structure you should use

    def train(self, trainingData, trainingLabels, validationData, validationLabels, allFeatures):
        """
        Outside shell to call your method. Do not modify this method.
        """
        self.features = allFeatures# dictionary with all valid features


        if (self.automaticTuning):
            iterationValues = [ 1, 3, 5, 10, 15, 20]
        else:
            iterationValues = [self.max_iterations]

        return self.trainAndTune(trainingData, trainingLabels, validationData, validationLabels, iterationValues)

    def trainAndTune(self, trainingData, trainingLabels, validationData, validationLabels, iterationValues):
        """
        The training loop for the perceptron passes through the training data several
        times and updates the weight vector for each label based on classification errors.
        See the project description for details.

        You should use the provided self.weights[label] data structure so that
        the classify method works correctly. Also, recall that a
        datum is a counter from features to values for those features
        (and thus represents a vector of values).

        iterationValues is a list of numbers of iterations to optimize over; you'll use
        accuracy on the validationData to decide which number of iteration is best.

        As an example, to loop through the training data iter times, you would do:
        for iter in range(iteration):
          print "Starting iteration ", iteration, "..."
          for i in range(len(trainingData)):
             #your code for updating based on trainingData[i] goes here.
        """
        '''# YOUR CODE HERE'''
        accuracy_dict = util.Counter()
        temp_weights = {}  # {(label, iteration) : {feature: value}} for self.max_iterations
        for iteration in iterationValues:
            # 1. Start with all weights = 0
            for i in range (iteration):
                # 2. Pick up training examples one by one
                for i in range(len(trainingData)):
                    score_f_y_dict = util.Counter()
                    # 3. Predict with the current weights
                    for label in self.legalLabels:
                        for feature in self.features:
                            score_f_y_dict[label] += (trainingData[i][feature] * self.weights[label][feature])
                    max_label = score_f_y_dict.argMax() # most likely label for given features

                    if max_label == trainingLabels[i]:
                        continue
                    else:
                        # Make correct weight vector similar to feature
                        # And make incorrect weight vector not similar to feature
                        self.weights[trainingLabels[i]] += trainingData[i]
                        self.weights[max_label] -= trainingData[i]

            # Finish training, now we have self.weights
            guesses = self.classify(validationData)
            correct_guess_num = 0.0
            for i in range(len(validationLabels)):
                if guesses[i] == validationLabels[i]:
                    correct_guess_num += 1.0
            accuracy_dict[iteration] = float(correct_guess_num) / float(len(validationLabels))
            temp_weights[iteration] = self.weights

        tmp_largest_key = accuracy_dict.argMax() # returns the iteration with the highest accuracy
        self.max_iterations = tmp_largest_key

        # recalculate the weights for the best iteration
        for i in range(self.max_iterations):
            for i in range(len(trainingData)):
                score_f_y_dict = util.Counter()
                # 3. Predict with the current weights
                for label in self.legalLabels:
                    for feature in self.features:
                        score_f_y_dict[label] += (trainingData[i][feature] * self.weights[label][feature])
                max_label = score_f_y_dict.argMax()  # most likely label for given features

                if max_label == trainingLabels[i]:
                    continue
                else:
                    # Make correct weight vector similar to feature
                    # And make incorrect weight vector not similar to feature
                    self.weights[trainingLabels[i]] += trainingData[i]
                    self.weights[max_label] -= trainingData[i]


    def classify(self, data):
        """
        Classifies each datum as the label that most closely matches the prototype vector
        for that label.  See the project description for details.

        Recall that a datum is a util.counter... Do not modify this method.
        """
        guesses = []

        '''# YOUR CODE HERE'''
        for datum in data:
            score_f_y_dict = util.Counter()
            # 3. Predict with the current weights
            for label in self.legalLabels:
                for feature in self.features:
                    score_f_y_dict[label] += (datum[feature] * self.weights[label][feature])
            max_label = score_f_y_dict.argMax()  # most likely label for given features
            guesses.append(max_label)
        return guesses


    def findHighOddsFeatures(self, class1, class2):
        """
        Returns:
        featuresClass1 -- the 100 largest weight features for class1 (as a list)
        featuresClass2 -- the 100 largest weight features for class2
        featuresOdds -- the 100 best features for difference in feature values
                         w_class1 - w_class2

        """

        featuresClass1 = []
        featuresClass2 = []
        featuresOdds = []

        '''## YOUR CODE HERE'''
        # find features for featuresClass1 and featuresClass2
        sorted_weights_class1 = self.weights[class1].sortedKeys()
        featuresClass1 = sorted_weights_class1[0:99]
        sorted_weights_class2 = self.weights[class2].sortedKeys()
        featuresClass2 = sorted_weights_class2[0:99]

        # find features for featuresOdds
        odds = util.Counter()
        for feature in self.features:
            odds[feature] = self.weights[class1][feature] - self.weights[class2][feature]
        sorted_odds = sorted(odds, key=odds.get)
        sorted_odds.reverse()
        featuresOdds = sorted_odds[0:99]

        return featuresClass1, featuresClass2, featuresOdds

