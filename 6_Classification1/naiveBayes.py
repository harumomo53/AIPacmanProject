import util
import classificationMethod
import math
from copy import deepcopy

class NaiveBayesClassifier(classificationMethod.ClassificationMethod):
    """
    See the project description for the specifications of the Naive Bayes classifier.
  
    Note that the variable 'datum' in this code refers to a counter of features
    (not to a raw samples.Datum).
    """
    def __init__(self, legalLabels):
        # DO NOT DELETE or CHANGE any of those variables!
        self.legalLabels = legalLabels
        self.type = "naivebayes"
        self.k = 1 # this is the smoothing parameter
        self.automaticTuning = False # Flat for automatic tuning of the parameters
        ''' YOUR CODE HERE '''
        # key = legalLabel, value = count
        self.priors_count = util.Counter()
        # key = legalLabel, value = p(y)
        self.priors_prob = util.Counter()
        # key = (label, feature, feature value), value = count (assume that feature value is binary for now)
        self.conditionals_count = util.Counter()
        # key = (label, feature, feature value), value = probability
        self.conditionals_prob = util.Counter()


    def setSmoothing(self, k):
        """
        This is used by the main method to change the smoothing parameter before training.
        Do not modify this method.
        """
        self.k = k

    def train(self, trainingData, trainingLabels, validationData, validationLabels, allFeatures):
        """
        Outside shell to call your method. Do not modify this method.
        """
        self.features = allFeatures# dictionary with all valid features - likely to be useful to you


        if (self.automaticTuning):
            kgrid = [0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 20, 50]
        else:
            kgrid = [self.k]
        
        return self.trainAndTune(trainingData, trainingLabels, validationData, validationLabels, kgrid)
      
    def trainAndTune(self, trainingData, trainingLabels, validationData, validationLabels, kgrid):
        """
        Train the classifier by collecting counts over the training data
        and choose the smoothing parameter among the choices in kgrid by
        using the validation data. This method should store the right parameters
        as a side-effect and should return the best smoothing parameters.

        See the project description for details.

        Note that trainingData is a list of feature Counters.

        You can get all the features that are possible from self.features, which stores
        a dictionary of features. You cannot guarantee that all features will actually be
        keys in the counter of features.

        If you want to simplify your code, you can assume that each feature is binary
        (can only take the value 0 or 1).

        You should also keep track of the priors (p(y) for each value of y) and conditional probabilities
        p(f_{i} | y) for each feature and class for further usage in calculateLogJointProbabilities method
        """
    
        '''## YOUR CODE HERE'''
        # Counting
        for i in range (len(trainingData)):
            self.priors_count[trainingLabels[i]] += 1
            for feature in self.features:
                if trainingData[i][feature] == 0:
                    self.conditionals_count[(trainingLabels[i], feature, 0)] += 1
                else:
                    self.conditionals_count[(trainingLabels[i], feature, 1)] += 1

        # Compute prior probability distribution
        for pkey, pval in self.priors_count.items():
            self.priors_prob[pkey] = float(pval) / float(len(trainingLabels))

        accuracy_dict = util.Counter() # key : kNum , value : accuracy
        # Compute conditional probability distribution
        for kNum in kgrid:
            for feature in self.features:
                for label in self.legalLabels:
                    # Count the number of cases that satisfy label, feature and feature value simultaneously
                    count_sum = self.conditionals_count[(label, feature, 0)] + self.conditionals_count[(label, feature, 1)]
                    # p(f|y) = p(f,y)/p(y)
                    self.conditionals_prob[(label, feature, 0)] = \
                        (float(self.conditionals_count[(label, feature, 0)]) + float(kNum)) / (float(count_sum) + float(kNum)*2)
                    self.conditionals_prob[(label, feature, 1)] = \
                        (float(self.conditionals_count[(label, feature, 1)]) + float(kNum)) / (float(count_sum) + float(kNum)*2)

            # Compute correctness
            guesses = self.classify(validationData)
            correct_guess_num = 0.0
            for i in range(len(validationLabels)):
                if guesses[i] == validationLabels[i]:
                    correct_guess_num += 1.0
            accuracy_dict[kNum] = float(correct_guess_num)/float(len(validationLabels))

        tmp_largest_key = accuracy_dict.argMax()

        max_accuracy = accuracy_dict[tmp_largest_key]
        min_k = float('inf')
        for key, val in accuracy_dict.items():
            if val == max_accuracy:
                if key < min_k:
                    min_k = key
        self.k = min_k

        # Recompute conditional probability distribution with optimal k
        for label in self.legalLabels:
            for feature in self.features:
                count_sum = self.conditionals_count[(label, feature, 0)] + self.conditionals_count[(label, feature, 1)]
                self.conditionals_prob[(label, feature, 0)] = \
                    (float(self.conditionals_count[(label, feature, 0)]) + float(self.k)) / (float(count_sum) + float(self.k)*2)
                self.conditionals_prob[(label, feature, 1)] = \
                    (float(self.conditionals_count[(label, feature, 1)]) + float(self.k)) / (float(count_sum) + float(self.k)*2)

        return self.k


    def classify(self, testData):
        """
        Classify the data based on the posterior distribution over labels.
    
        You shouldn't modify this method.
        """
        guesses = []
        self.posteriors = [] # Log posteriors are stored for later data analysis (autograder).
        for datum in testData:
            posterior = self.calculateLogJointProbabilities(datum)
            guesses.append(posterior.argMax())
            self.posteriors.append(posterior)
        return guesses
      
    def calculateLogJointProbabilities(self, datum):
        """
        Returns the log-joint distribution over legal labels and the datum.
        Each log-probability should be stored in the log-joint counter, e.g.
        logJoint['face'] = <Estimate of log( P(Label = face, datum) )>
        """
        logJoint = util.Counter()
    
        '''## YOUR CODE HERE'''
        # init logJoint
        for label in self.legalLabels:
            logJoint[label] = math.log(self.priors_prob[label])
            for feature in self.features:
                logJoint[label] += math.log(self.conditionals_prob[(label, feature, datum[feature])])

        return logJoint

  
    def findHighOddsFeatures(self, class1, class2):
        """
        Returns:
        featuresClass1 -- the 100 best features for P(feature=on|class1) (as a list)
        featuresClass2 -- the 100 best features for P(feature=on|class2)
        featuresOdds -- the 100 best features for the odds ratio
                     P(feature=on|class1)/P(feature=on|class2) 
        """

        featuresClass1 = []
        featuresClass2 = []
        featuresOdds = []
    
        '''## YOUR CODE HERE'''
        sortedKeys = self.conditionals_prob.sortedKeys()
        for i in range(len(sortedKeys)):
            if len(featuresClass1) == 100 and len(featuresClass2) == 100:
                break
            else:
                curTuple = sortedKeys[i]
                if curTuple[0] == class1 and curTuple[2] == 1:
                    featuresClass1.append(curTuple[1])
                elif curTuple[0] == class2 and curTuple[2] == 1:
                    featuresClass2.append(curTuple[1])

        oddsDict = util.Counter()
        for feature in self.features:
            oddsDict[feature] = self.conditionals_prob[(class1, feature, 1)] / self.conditionals_prob[(class2, feature, 1)]
        sortedOdds = oddsDict.sortedKeys()
        sortedKeys.reverse()
        featuresOdds = sortedOdds[0:99]

        return featuresClass1,featuresClass2,featuresOdds
    

    
      
