Building Classifiers
===

I designed 2 classifiers: a naive Bayes classifier and a perceptron classifier. I tested my classifiers on 2 datasets: a set of face images in which edges have already been detected and a set of words/phrases that are either cheeses or diseases.

- Face detection: determine whether the edge is a face or not
- Text classification: determine whether the word or phrase of the name is name of a cheese or name of a disease.


## Features
### Face data
- Face and non-face images of size 60x70 pixels, giving a vector of 4200 features for each item.
- These features can take values of 0 or 1 depending on whether there was an edge detected at each pixel

### Cheese/Disease data
- Each instance in the data is a word or phrase
- The default features are 0 or 1 for whether each letter of the alphabet is in the word
- 0 or 1 for whether each 2-character sequence is in the word
- e.g. For word 'and', the features have value 1:'a','n','d','an','nd' (features that are not present are not keys in the counter)


## Q1, Q2. Naive Bayes
### Theory
A naive Bayes classifier models a **joint distribution** over a label *Y* and a set of observed random variables or features using the assumption that **the full joint distribution can be represented as a Bayes net with a node for the variable Y and a directed edge from Y to each feature Fi**

- To classify an instance, we find the **most probable class given the feature values for that instance**
- **log probabilities**: because multiplying many probabilities together can result in **underflow** (probability too close to 0)

### Parameter Estimation
- Parameter #1: **prior distribution** over labels(cheese/disease, face/not-face) = estimated directly from the training data using counts
- Parameter #2: **conditional probabilities** of features given each label y *p(Fi|Y=y)*. = use counts from the training data to estimate these probabilities

### Smoothing
- **Laplace Smoothing**: To make sure that no parameter ever receives an estimate of zero

### Odds Ratios
- To understand the parameters better, we look at **odds ratios**. (most likely features for a given class)
- For each feature : *odds(Fi=on, y1, y2) = P(Fi=on|y1)/ P(Fi=on|y2)*
- The features that have the greatest impact at classification time are those with both a high probability (because they appear often in the data) and a high odds ratio (because they strongly bias one label versus another). 


## Q3. Q4. Percpetron
### Theory
Unlike the naive Bayes classifier, a perceptron does not use probabilities to make its decisions. Instead **it keeps a prototype weight vector wy**. Given a feature list *f*, the perceptron computes the class *y* whose prototype is most similar to the input vector *f*.

![Image of perceptron](https://github.com/eunjincho503/AIPacmanProject/blob/master/6_Classification1/img/perceptron1.png)
- Formally, given a feature vector *f*, we score each class with the equation above. Then we **choose the class with the highest score**.

### Learning weights
What we need is to learn the **prototype weights**. 

![Image of perceptron](https://github.com/eunjincho503/AIPacmanProject/blob/master/6_Classification1/img/perceptron2.png)
1. We scan over the data, one instance at a time. When we come to an instance (f, y), we find the label with the highest score.
2. We compare y' to the true label y. 
3. If y'= y, we do nothing.
4. Otherwise, the prototype wy needs to be more like *f* and the prototype wy' needs to be less like *f* to help prevent this error in the future.
![Image of perceptron](https://github.com/eunjincho503/AIPacmanProject/blob/master/6_Classification1/img/perceptron3.png)

### Visualizing weights

## Improvements
To increase classifier's accuracy further, I'll **extract more useful features from the data**.

- e.g. in the face data, you might think about enclosed white space being more or less likely to be present in faces versus non-faces. This is an example of a feature that is not directly available to the classifier from the per-pixel information
-  Trying lots of features and choosing the ones that are best on test gives you a way of peeking at information about the test data. 
