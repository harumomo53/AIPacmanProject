GhostBusters
===
Design of Pacman agents that use sensors to locate and eat invisible ghosts. The goal is to hunt down scared but invisible ghosts. Pacman is equipped with sonar ears that provides noisy readings of the Manhattan distance to each ghost. Implement algorithms for perfoming both **exact and approximate inference using Bayes' Nets** to track the ghosts.


## Exact Inference Observation
- Updated the ```observe``` method in ```ExactInference``` class of ```inference.py``` to correctly **update the agent's belief distribution over ghost positions given an observation from Pacman's sensors**.
- When ghost ghost is eaten, I also place that ghost in its prison cell and update belief distribution.

### How to run
- ```python autograder.py -q q1```
- ```python autograder.py -t test_cases/q1/1-ExactObserve```


## Exact Inference Time Elapse
- Update self.beliefs in response to a time step passing from the current state
- Implemented the ```elapseTime``` method in ```ExactInference```. My agent has access to the action distribution for any ```GhostAgent```.
- Pacman will start with a uniform distribution over all spaces, and then update his beliefs according to how he knows the Ghost is able to move.
- Since Pacman is not observing the ghost, this means the ghost's actions will not impact Pacman's beliefs

### How to run
- ```python autograder.py -q q2```


## Exact Inference Full System
- Now I'll use my ```observe``` and ```elapseTime``` implementations together, along with a simple **greedy hunting strategy**. In this strategy, Pacman assumes that **each ghost is in its most likely position** according to its beliefs, then moves toward the closest ghost.
- Implemented the ```chooseAction``` method in ```GreedyBustersAgent``` in ```bustersAgents.py```.
- First find the most likely position of each remaining (uncaptured) ghost, then choose an action that minimizes the distance to the closest ghost. 

### How to run
- ```python autograder.py -q q3```


## Approximate Inference Observation (Particle Filtering)
- Implemented the functions ```initializeUniformly```, ```getBeliefDistribution```, and ```observe``` for the ```ParticleFilter``` class in ```inference.py```.
- There are 2 things to consider:
    - When all your particles receive **zero weight** based on the evidence, you should **resample all particles** from the prior to recover
    - When a ghost is eaten, you should update all particles to place that ghost in its prison cell.

### Things I needed to keep in mind
- I should create a global variable such as ```self.particlesList``` that keeps track of samples(particles)

### How to run
- ```python autograder.py -q q4```


## Approximate Infernce Time Elapse
- Implement the ```elapseTime``` function for the ```ParticleFilter``` class in ```inference.py```.

### How to run
- ```python autograder.py -q q5```


## Joint Particle Filter Observation
For both exact Inference and approximate inference, we have tracked each ghost independently. Now, ```DispersingGhost``` chooses actions that avoid other ghosts, so the ghosts' transition models are no longer independent. All ghosts must be tracked jointly in a **dynamic Bayes Net**.

- Implement a particle filter that tracks multiple ghosts simultaneously.
- Each particle = a tuple of ghost positions that is a sample of where all the ghosts are at the present time
- To display **belief clouds** about individual ghosts, we need to extract **marginal distributions** about each ghost from the **joint inference algorithm**.

### To do
- Complete the ```initializeParticles```, ```getBeliefDistribution```, and ```observeState``` method in ```JointParticleFilter``` class : it is to weight and resample the whole list of particles based on new evidence

### How to run
- ```python autograder.py -q q6```
-q q6 --no-graphics


## Joint Particle Filter Elapse Time  
Samples each particle's next state based on its current state and the gameState.

- Complete the ```elapseTime``` method in ```JointParticleFilter``` in ```inference.py``` to **resample each particle correclty for the BN**.
- Each ghost should draw a new position conditioned on the positions of all the ghosts at the previous time step.









